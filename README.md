# Linux installers for cmdock

## Ubuntu and Debian installer

To build installer docker image run following command:
`sudo docker build -tcmdock_debian12 --build-arg version=debian:bookworm .`
and replace version={version} with desired version 

* ubuntu:focal 
* ubuntu:jammy
* debian:buster
* debian:bookworm

to run the installer build execute following command
sudo docker run -v.:/m cmdock_debian12
